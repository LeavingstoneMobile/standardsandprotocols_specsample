# JSONMessaging baser service documentation

კლონირებისთვის გამოიყენე რეკურსიული კლონი


```
git clone --recursive https://bitbucket.org/LeavingstoneMobile/standardsandprotocols_specsample.git
```

რომელიმე სერვისი დოკუმენტაციის საწყისი ფაილების შესაქმენლად გაუშვი ბრძანება
```
                               Project name     
                                    |       API name          
                                    |           |
 node cli/createServiceDoc.js SomeProject UserManagement 2
```